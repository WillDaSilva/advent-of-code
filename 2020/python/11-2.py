with open('11.in') as f:
    grid = [[None if x == '.' else x == '#' for x in y.strip()] for y in f]

directions = ((0, 1), (0, -1), (1, 0), (-1, 0), (1, 1), (1, -1), (-1, 1), (-1, -1))
def count_occupied(x, y):
    occupied = 0
    for (dx, dy) in directions:
        d = 1
        while True:
            if not(0 <= x + dx * d < len(grid[0]) and 0 <= y + dy * d < len(grid)):
                break
            elif old_grid[y + dy * d][x + dx * d] is not None:
                occupied += old_grid[y + dy * d][x + dx * d] is True
                break
            d += 1
    return occupied

seen = set()
while (old_grid := tuple(tuple(y) for y in grid)) not in seen:
    seen.add(old_grid)
    for y in range(len(grid)):
        for x in range(len(grid[0])):
            if grid[y][x] is None:
                continue
            occupied = count_occupied(x, y)
            grid[y][x] = occupied < 5 if old_grid[y][x] else not occupied
print(sum(grid[y][x] is True for y in range(len(grid)) for x in range(len(grid[0]))))
