with open('16.in') as f:
    notes = f.read().split('\n\n')

class TicketField:
    def __init__(self, x):
        self.ranges = []
        for r in x.split(' or '):
            a, b = r.split('-')
            self.ranges.append(range(int(a), int(b) + 1))
    def __contains__(self, x):
        return any(x in r for r in self.ranges)

ranges = {a: TicketField(b) for a, b in [x.split(': ') for x in notes[0].splitlines()]}
my_ticket = [int(x) for x in notes[1].splitlines()[1].split(',')]
nearby_tickets = [[int(y) for y in x.split(',')] for x in notes[2].splitlines()[1:]]

print(sum(b for a in nearby_tickets for b in a if not any(b in x for x in ranges.values())))
