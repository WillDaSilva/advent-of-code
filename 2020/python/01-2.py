with open('01.in') as f:
    data = sorted([int(x) for x in f])

for x in reversed(data):
    for y in data:
        for z in data:
            if x + y + z > 2020:
                break
            elif x + y + z == 2020:
                print(x * y * z)
                exit()

