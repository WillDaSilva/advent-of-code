with open('05.in') as f:
    passes = [(x[:-3], x[-3:]) for x in f.read().splitlines()]

tr = {ord('B'): '1', ord('F'): '0'}
tc = {ord('R'): '1', ord('L'): '0'}
ids = sorted(int(x.translate(tr), 2) * 8 + int(y.translate(tc), 2) for x, y in passes)
a = 0
b = len(ids) - 1
c = 0
while a + 1 < b:
    c = (a + b) // 2
    if ids[a] - a != ids[c] - c:
        b = c
    elif ids[b] - b != ids[c] - c:
        a = c
print(ids[c] - 1)
