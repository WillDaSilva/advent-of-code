from collections import deque

with open('22.in') as f:
    p1, p2 = (deque(int(y) for y in x.splitlines()[1:]) for x in f.read().split('\n\n'))

def game(p1, p2):
    p1_prev = set()
    p2_prev = set()
    while p1 and p2:
        p1t = tuple(p1)
        p2t = tuple(p2)
        if p1t in p1_prev or p2t in p2_prev:
            return (p1, True)
        else:
            p1_prev.add(p1t)
            p2_prev.add(p2t)
        c1, c2 = p1.popleft(), p2.popleft()
        if len(p1) >= c1 and len(p2) >= c2:
            if game(deque(list(p1)[:c1]), deque(list(p2)[:c2]))[1]:
                p1.extend((c1, c2))
            else:
                p2.extend((c2, c1))
        elif c1 > c2:
            p1.extend((c1, c2))
        else:
            p2.extend((c2, c1))
    return (p1, True) if p1 else (p2, False)

print(sum(i * x for i, x in enumerate(reversed(game(p1, p2)[0]), start=1)))
