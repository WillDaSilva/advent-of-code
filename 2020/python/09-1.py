from collections import deque
import itertools as it

with open('09.in') as f:
    stream = (int(x.strip()) for x in f.readlines())

window = deque(it.islice(stream, 25))

for item in stream:
    a = {item - x for x in window}
    if not any(x in a for x in window):
        print(item)
        break
    window.popleft()
    window.append(item)
