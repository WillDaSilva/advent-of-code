from collections import deque

with open('09.in') as f:
    data = [int(x.strip()) for x in f.readlines()]

window = deque(data[:25])
invalid = None
for item in data[25:]:
    a = {item - x for x in window}
    if not any(x in a for x in window):
        invalid = item
        break
    window.popleft()
    window.append(item)

window = deque()
stream = iter(data)
while True:
    s = sum(window)
    if s > invalid:
        window.popleft()
    elif s < invalid:
        window.append(next(stream))
    else:
        print(min(window) + max(window))
        break
