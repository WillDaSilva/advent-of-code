from sympy.ntheory.modular import crt

idx = 0
busses = []
with open('13.in') as f:
    f.readline() # ignore the first line of the input
    for x in f.readline().strip().split(','):
        if x != 'x':
            busses.append((idx, int(x)))
        idx += 1
print(crt((x for _, x in busses), [x - i for i, x in busses])[0])
