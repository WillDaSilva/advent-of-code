rotations = (
    lambda x, y: (x, y),
    lambda x, y: (y, -x),
    lambda x, y: (-x, -y),
    lambda x, y: (-y, x),
)

actions = {
    'N': lambda x, y, wx, wy, m: (x, y, wx, wy + m),
    'S': lambda x, y, wx, wy, m: (x, y, wx, wy - m),
    'E': lambda x, y, wx, wy, m: (x, y, wx - m, wy),
    'W': lambda x, y, wx, wy, m: (x, y, wx + m, wy),
    'L': lambda x, y, wx, wy, m: (x, y, *rotations[m // 90](wx, wy)),
    'R': lambda x, y, wx, wy, m: (x, y, *rotations[4 - m // 90](wx, wy)),
    'F': lambda x, y, wx, wy, m: (x + wx * m, y + wy * m, wx, wy),
}

with open('12.in') as f:
    instructions = ((actions[x[0]], int(x[1:])) for x in f.read().splitlines())

state = (0, 0, -10, 1) # [x, y, waypoint x, waypoint y]
for action, magnitude in instructions:
    state = action(*state, magnitude)
print(abs(state[0]) + abs(state[1]))
