from collections import defaultdict

with open('07.in') as f:
    rules = [[a, b.split(', ')] for a, b in [x.split(' contain ') for x in f.read().split('.\n')]]
    rules = [[' '.join(x.split(' ')[:-1]), [' '.join(z.split(' ')[1:-1]) for z in y]] for x, y in rules]

rules_map = defaultdict(set)
for outer, inner in rules:
    for colour in inner:
        rules_map[colour].add(outer)

seen = set()
to_visit = rules_map['shiny gold']
reachable = set()
while len(to_visit):
    current = to_visit.pop()
    to_visit |= rules_map[current] - seen
    seen |= rules_map[current]
    reachable.add(current)
print(len(reachable))
