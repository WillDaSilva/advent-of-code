valid = 0
with open('02.in') as f:
    for x, y in (x.split(': ') for x in f):
        a, b = x.split(' ')
        q, p = a.split('-')
        if (y[int(q) - 1] == b) ^ (y[int(p) - 1] == b):
            valid += 1
print(valid)
