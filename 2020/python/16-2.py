import itertools as it
from functools import reduce
from operator import mul

with open('16.in') as f:
    notes = f.read().split('\n\n')

class TicketField:
    def __init__(self, name, range_text):
        self.name = name
        self.ranges = []
        for r in range_text.split(' or '):
            a, b = r.split('-')
            self.ranges.append(range(int(a), int(b) + 1))
    def __contains__(self, x):
        return any(x in r for r in self.ranges)
    def __hash__(self):
        return hash(self.name)
    def __repr__(self):
        return f'Ticket<{self.name}>'

fields = {TicketField(a, b) for a, b in [x.split(': ') for x in notes[0].splitlines()]}
my_ticket = [int(x) for x in notes[1].splitlines()[1].split(',')]
nearby_tickets = [[int(y) for y in x.split(',')] for x in notes[2].splitlines()[1:]]
invalid_idx = {i for i, a in enumerate(nearby_tickets) for b in a if not any(b in x for x in fields)}
undetermined = zip(*(x for i, x in enumerate(nearby_tickets) if i not in invalid_idx))
undetermined = [(i, x, {x for x in fields}) for i, x in enumerate(undetermined)]

determined = set()
cycle = it.cycle(undetermined)
while len(undetermined):
    i, values, possibilities = next(cycle)
    possibilities -= {r for r in fields if not all(x in r for x in values)}
    if len(possibilities) == 1:
        actual = possibilities.pop()
        determined.add((i, actual))
        undetermined.remove((i, values, possibilities))
        for _, _, c in undetermined:
            c.remove(actual)
        cycle = it.cycle(undetermined)
print(reduce(mul, (my_ticket[i] for i, x in determined if x.name.startswith('departure'))))
