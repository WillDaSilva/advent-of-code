with open('10.in') as f:
    adapters = sorted(int(x) for x in f)

a = 1
b = 1
for i in range(1, len(adapters)):
    if adapters[i] > adapters[i-1] + 4:
        break
    a += adapters[i] - adapters[i-1] == 1
    b += adapters[i] - adapters[i-1] == 3
print(a * b)
