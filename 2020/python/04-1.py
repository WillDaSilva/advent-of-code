with open('04.in') as f:
    text = f.read()

passports = [dict(y.split(':') for y in x.split()) for x in text.split('\n\n')]
valid = 0
for passport in passports:
    valid += not bool(
        {'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'} - set(passport))
print(valid)