from functools import reduce
import itertools as it
from graph_tools import Graph
import numpy as np
from operator import mul

def border_hashes(x, all_orientations=False):
    if all_orientations:
        return tuple(border.dot(1 << np.arange(10)) for y in orientations(x) for border in borders(y))
    return tuple(border.dot(1 << np.arange(10)) for border in borders(x))

def borders(x):
    return (x[0], x[-1], x.T[0], x.T[-1])

def center(a):
    return a[1:-1, 1:-1]

def orientations(a):
    yield a             # e
    for _ in range(4):
        a = np.rot90(a)
        yield a         # R^k
    a = np.flipud(a)
    yield a             # T
    for _ in range(4):
        a = np.rot90(a)
        yield a         # TR^k

tiles = {}
with open('20.in') as f:
    for tile in f.read().split('\n\n'):
        id_row, *rows = tile.splitlines()
        tid = int(id_row[5:-1])
        tiles[tid] = np.array([[x == '#' for x in y] for y in rows], dtype=np.uint8)

g = Graph()
tid_to_border_hashes = {tid: border_hashes(tile, True) for tid, tile in tiles.items()}
for x, y in it.combinations(tid_to_border_hashes, 2):
    if len(set(tid_to_border_hashes[x]) & set(tid_to_border_hashes[y])) == 2:
        g.add_edge(x, y)
print(reduce(mul, (n for n in g.vertices() if len(g.neighbors(n)) == 2)))
