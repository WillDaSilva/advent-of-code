from functools import reduce
import itertools as it
import operator as op

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
    def __iter__(self):
        yield self.data
        x = self
        while x := x.next:
            yield x.data

def game(s, num_steps, num_cups):
    nodes = {x: Node(x) for x in range(1, num_cups + 1)}
    for x, y in zip(s, s[1:]):
        nodes[x].next = nodes[y]
    if len(s) == num_cups:
        nodes[s[-1]].next = nodes[s[0]]
    else:
        nodes[s[-1]].next = nodes[len(s) + 1]
        for i in range(len(s) + 1, num_cups):
            nodes[i].next = nodes[i + 1]
        nodes[num_cups].next = nodes[s[0]]
    head = nodes[s[0]]
    for _ in range(num_steps):
        a = head.next
        b = head.data
        while (b := b - 1 or num_cups) in (a.data, a.next.data, a.next.next.data): pass
        head.next = a.next.next.next
        a.next.next.next = nodes[b].next
        nodes[b].next = a
        head = head.next
    return nodes[1]

with open('23.in') as f:
    s = [int(x) for x in f.read()]
print(reduce(op.mul, it.islice(game(s, 10000000, 1000000), 1, 3)))
