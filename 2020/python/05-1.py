with open('05.in') as f:
    passes = [(x[:-3], x[-3:]) for x in f.read().splitlines()]

tr = {ord('B'): '1', ord('F'): '0'}
tc = {ord('R'): '1', ord('L'): '0'}
print(max(int(x.translate(tr), 2) * 8 + int(y.translate(tc), 2) for x, y in passes))
