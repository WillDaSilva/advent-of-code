with open('11.in') as f:
    grid = [[None if x == '.' else x == '#' for x in y.strip()] for y in f]

offsets = ((0, 1), (0, -1), (1, 0), (-1, 0), (1, 1), (1, -1), (-1, 1), (-1, -1))
def adjacent(x, y):
    yield from ((x + dx, y + dy) for dx, dy in offsets if 0 <= x + dx < len(grid[0]) and 0 <= y + dy < len(grid))

seen = set()
while (old_grid := tuple(tuple(y) for y in grid)) not in seen:
    seen.add(old_grid)
    for y in range(len(grid)):
        for x in range(len(grid[0])):
            if grid[y][x] is None:
                continue
            occupied = sum(old_grid[ay][ax] is True for ax, ay in adjacent(x, y))
            grid[y][x] = occupied < 4 if old_grid[y][x] else not occupied
print(sum(grid[y][x] is True for y in range(len(grid)) for x in range(len(grid[0]))))
