from functools import reduce
from operator import mul

with open('03.in') as f:
    grid = [[x == '#' for x in y.strip()] for y in f]

def f(dx, dy):
    x, y = 0, 0
    hit = 0
    while y < len(grid):
        hit += grid[y][x]
        x = (x + dx) % len(grid[0])
        y += dy
    return hit

print(reduce(mul, (f(dx, dy) for dx, dy in ((1, 1), (3, 1), (5, 1), (7, 1), (1, 2)))))
