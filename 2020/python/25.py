with open('25.in') as f:
    a, b = (int(x) for x in f.read().splitlines())

def powers(x, modulo):
    base = x
    yield 1
    yield x
    while True:
        yield (x := (x * base) % modulo)

print(pow(a, next(i for i, x in enumerate(powers(7, 20201227)) if x == b), 20201227))
