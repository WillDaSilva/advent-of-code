from string import digits, hexdigits

with open('04.in') as f:
    text = f.read()

passports = [dict(y.split(':') for y in x.split()) for x in text.split('\n\n')]
valid = 0
for passport in passports:
    valid += \
        not bool({'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'} - set(passport)) \
        and 1920 <= int(passport['byr']) <= 2002 \
        and 2010 <= int(passport['iyr']) <= 2020 \
        and 2020 <= int(passport['eyr']) <= 2030 \
        and {
                'cm': 150 <= int(passport['hgt'][:-2]) <= 193,
                'in': 59 <= int(passport['hgt'][:-2]) <= 76
            }.get(passport['hgt'][-2:], False) \
        and passport['hcl'].startswith('#') \
        and all(x in hexdigits for x in passport['hcl'][1:]) \
        and passport['ecl'] in ('amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth') \
        and len(passport['pid']) == 9 \
        and all(x in digits for x in passport['pid'])
print(valid)