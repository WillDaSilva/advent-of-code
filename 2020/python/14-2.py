from collections import defaultdict
import itertools as it
import re

with open('14.in') as f:
    instructions = f.read().splitlines()

def mask_addr(addr, mask):
    floating_bit_indices = []
    for i, x in enumerate(reversed(mask)):
        if x == '1':
            addr |= 2**i
        elif x == 'X':
            floating_bit_indices.append(i)
    for set_bits in it.product(range(2), repeat=len(floating_bit_indices)):
        ones_mask = 0
        zeros_mask = 0
        for i, bit in enumerate(set_bits):
            if bit:
                ones_mask |= 2**floating_bit_indices[i]
            else:
                zeros_mask |= 2**floating_bit_indices[i]
        yield (addr | ones_mask) & ~zeros_mask

memory = defaultdict(int)
mask = None
for instruction in instructions:
    if instruction.startswith('mask'):
        mask = instruction.split(' = ')[1]
    else:
        base_addr, val = (int(x) for x in re.match(r'mem\[(\d+)\] = (\d+)', instruction).groups())
        for addr in mask_addr(base_addr, mask):
            memory[addr] = val
print(sum(memory.values()))
