import itertools as it

d = {
    'e': (1, -1, 0),
    'w': (-1, 1, 0),
    'ne': (1, 0, -1),
    'nw': (0, 1, -1),
    'se': (0, -1, 1),
    'sw': (-1, 0, 1)
}

def split(s):
    i = 0
    while i < len(s):
        if s[i] in ('e', 'w'):
            yield s[i]
            i += 1
        else:
            yield s[i:i+2]
            i += 2

with open('24.in') as f:
    tiles = [[d[y] for y in split(x)] for x in f.read().splitlines()]

state = set()
for tile in tiles:
    state ^= {tuple(sum(a) for a in zip(*tile))}

def neighbors(coord):
    for p in d.values():
        if all(v == 0 for v in p):
            continue
        yield tuple(v + dv for v, dv in zip(coord, p))

for i in range(100):
    prev_state = state.copy()
    for coord in (y for x in prev_state for y in (x, *neighbors(x))):
        v = sum(neighbor in prev_state for neighbor in neighbors(coord))
        a = coord in state
        if a and (v == 0 or v > 2):
            state.remove(coord)
        elif (not a) and v == 2:
            state.add(coord)
print(len(state))
