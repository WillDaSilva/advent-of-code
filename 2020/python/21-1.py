from collections import defaultdict

with open('21.in') as f:
    foods = [x[:-1].split(' (contains ') for x in f.read().splitlines()]
    foods = [(set(a.split()), b.split(', ')) for a, b in foods]

safe = set()
possible_sources = defaultdict(lambda: ingredients.copy())
for ingredients, allergens in foods:
    safe |= ingredients
    for allergen in allergens:
        possible_sources[allergen] &= ingredients
safe -= {y for x in possible_sources.values() for y in x}
print(sum(sum(ingredient in safe for ingredient in ingredients) for ingredients, _ in foods))
