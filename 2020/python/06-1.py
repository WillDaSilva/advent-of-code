from functools import reduce

with open('06.in') as f:
    answers = [x.split() for x in f.read().split('\n\n')]

print(sum(len(reduce(lambda x, y: x | set(y), group, set())) for group in answers))
