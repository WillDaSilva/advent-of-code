import operator as op
import re

with open('18.in') as f:
    expressions = f.read().splitlines()

def evalx(s):
    while '(' in s:
        paren_layer = 1
        start = s.index('(')
        end = start
        while paren_layer:
            end += 1
            if s[end] == ')':
                paren_layer -= 1
            elif s[end] == '(':
                paren_layer += 1
        s = f'{s[:start]}{evalx(s[start+1:end])}{s[end+1:]}'
    return calc(s)

def calc(s):
    try:
        return int(s)
    except ValueError:
        pass
    if '*' in s:
        tokens = iter(re.split(r'(\*)', s))
    else:
        tokens = iter(re.split(r'(\+)', s))
    left = calc(next(tokens))
    for operator, right in zip(tokens, tokens):
        left = {'*': op.mul, '+': op.add}[operator](calc(left), calc(right))
    return left

print(sum(evalx(expression) for expression in expressions))
