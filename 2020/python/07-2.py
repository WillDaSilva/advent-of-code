from functools import lru_cache

with open('07.in') as f:
    rules = [[a, b.split(', ')] for a, b in [x.split(' contain ') for x in f.read().split('.\n')]]
    rules = [[(' '.join(x.split(' ')[:-1])), [' '.join(z.split(' ')[:-1]) for z in y]] for x, y in rules]
    extract_num = lambda z: (int(' '.join(z.split(' ')[0])), ' '.join(z.split(' ')[1:]))
    rules = dict([x, [extract_num(z) for z in y if not z.startswith('no')]] for x, y in rules)

@lru_cache
def num_bags(outer):
    contains = rules[outer]
    if not contains:
        return 1
    return 1 + sum(x * num_bags(y) for x, y in contains)

print(num_bags('shiny gold') - 1)
