import itertools as it
from graph_tools import Graph
from math import sqrt
import numpy as np

def border_hashes(x, all_orientations=False):
    if all_orientations:
        return tuple(border.dot(1 << np.arange(10)) for y in orientations(x) for border in borders(y))
    return tuple(border.dot(1 << np.arange(10)) for border in borders(x))

def borders(x):
    return (x[0], x[-1], x.T[0], x.T[-1])

def center(a):
    return a[1:-1, 1:-1]

def orientations(a):
    yield a             # e
    for _ in range(4):
        a = np.rot90(a)
        yield a         # R^k
    a = np.flipud(a)
    yield a             # T
    for _ in range(4):
        a = np.rot90(a)
        yield a         # TR^k

tiles = {}
with open('20.in') as f:
    for tile in f.read().split('\n\n'):
        id_row, *rows = tile.splitlines()
        tid = int(id_row[5:-1])
        tiles[tid] = np.array([[x == '#' for x in y] for y in rows], dtype=np.uint8)

g = Graph()
tid_to_border_hashes = {tid: border_hashes(tile, True) for tid, tile in tiles.items()}
for x, y in it.combinations(tid_to_border_hashes, 2):
    if len(set(tid_to_border_hashes[x]) & set(tid_to_border_hashes[y])) == 2:
        g.add_edge(x, y)
corners = [n for n in g.vertices() if len(g.neighbors(n)) == 2]

def build_image(x, y):
    tid, a = image_cells[(x, y)]
    for neighbor_id in g.neighbors(tid):
        if neighbor_id not in {tid for tid, _ in image_cells.values()}:
            for ori in orientations(tiles[neighbor_id]):
                if border_hashes(ori)[0] == border_hashes(a)[1]:
                    image_cells[(x + 1, y)] = (neighbor_id, ori)
                if border_hashes(ori)[2] == border_hashes(a)[3]:
                    image_cells[(x, y + 1)] = (neighbor_id, ori)

starting_tid = corners.pop()
for x in orientations(tiles[starting_tid]):
    image_cells = {}
    image_cells[(0, 0)] = (starting_tid, x)
    build_image(0, 0)
    if len(image_cells) == 3:
        break

n = int(sqrt(len(tiles)))
image = np.zeros((n*8, n*8), dtype=np.uint8)
for x, y in it.product(range(n), repeat=2):
    build_image(x, y)
    image[x*8:(x+1)*8, y*8:(y+1)*8] = center(image_cells[(x, y)][1])

monster = np.array(
    [[c == "#" for c in l] for l in ('                  # ',
                                     '#    ##    ##    ###',
                                     ' #  #  #  #  #  #   ')],
    dtype=np.uint8
)
for image in orientations(image):
    my, mx = monster.shape
    if mc := sum((monster == image[y:y+my, x:x+mx] & monster).all()
                    for x, y in it.product(range(image.shape[1] - mx + 1), range(image.shape[0] - my + 1))):
        print(int(image.sum() - mc * monster.sum()))
        break
