with open('13.in') as f:
    time = int(f.readline().strip())
    busses = [int(x) for x in f.readline().strip().split(',') if x != 'x']

print((a := min((x - time % x, x) for x in busses))[0] * a[1])
