from collections import defaultdict
import itertools as it

state = defaultdict(bool)

with open('17.in') as f:
    for y, line in enumerate(f.read().splitlines()):
        for x, item in enumerate(line):
            state[(x, y, 0, 0)] = item == '#'

def neighbors(coord):
    for p in it.product(range(-1, 2), repeat=len(coord)):
        if all(v == 0 for v in p):
            continue
        yield tuple(v + dv for v, dv in zip(coord, p))

for _ in range(6):
    old_state = state.copy()
    xr = range(min(state, key=lambda x: x[0])[0] - 1, max(state, key=lambda x: x[0])[0] + 2)
    yr = range(min(state, key=lambda x: x[1])[1] - 1, max(state, key=lambda x: x[1])[1] + 2)
    zr = range(min(state, key=lambda x: x[2])[2] - 1, max(state, key=lambda x: x[2])[2] + 2)
    wr = range(min(state, key=lambda x: x[3])[3] - 1, max(state, key=lambda x: x[3])[3] + 2)
    for coord in it.product(xr, yr, zr, wr):
        active_neighbors = sum(old_state[p] for p in neighbors(coord))
        if old_state[coord] and active_neighbors not in (2, 3):
            state[coord] = False
        elif not old_state[coord] and active_neighbors == 3:
            state[coord] = True

print(sum(state.values()))
