from collections import defaultdict
import re

with open('14.in') as f:
    instructions = f.read().splitlines()

def split_mask(mask):
    ones_mask = 0
    zeros_mask = 0
    for i, x in enumerate(reversed(mask)):
        if x == '1':
            ones_mask |= 2**i
        elif x == '0':
            zeros_mask |= 2**i
    return ones_mask, zeros_mask

memory = defaultdict(int)
ones_mask = 0
zeros_mask = 0
for instruction in instructions:
    if instruction.startswith('mask'):
        ones_mask, zeros_mask = split_mask(instruction.split(' = ')[1])
    else:
        addr, val = (int(x) for x in re.match(r'mem\[(\d+)\] = (\d+)', instruction).groups())
        memory[addr] = (val | ones_mask) & ~zeros_mask
print(sum(memory.values()))
