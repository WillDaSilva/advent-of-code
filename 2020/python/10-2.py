from functools import lru_cache
import itertools as it

with open('10.in') as f:
    adapters = sorted(int(x) for x in f)

@lru_cache
def traverse(idx):
    if idx == len(adapters) - 1:
        return 1
    steps = range(1, min(4, len(adapters) - idx)) # avoids going over end of list
    return sum(traverse(idx + x) for x in steps if adapters[idx + x] - adapters[idx] < 4)

print(sum(traverse(i) for i, _ in it.takewhile(lambda x: x[1] < 4, enumerate(adapters))))
