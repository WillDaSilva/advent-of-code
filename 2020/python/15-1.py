with open('15.in') as f:
    starting_numbers = [int(x) for x in f.read().strip().split(',')]

d = {}

turn = 1
for x in starting_numbers:
    d[x] = (x not in d, turn, None)
    turn += 1
last = starting_numbers[-1]
for _ in range(2020 - len(starting_numbers)):
    if d[last][0]:
        last = 0
    else:
        last = d[last][1] - d[last][2]
    d[last] = (last not in d, turn, d[last][1] if last in d else None)
    turn += 1
print(last)
