with open('19.in') as f:
    rules, messages = (x.split('\n') for x in f.read().split('\n\n'))

rules = [x.split(': ') for x in rules]
rules = {a: b[1:-1] if b[0] == '"' else [x.split() for x in b.split(' | ')] for a, b in rules}
rules = {
    **rules,
    '8': [['42'], ['42', '8']],
    '11': [['42', '31'], ['42', '11', '31']]
}

def parse(rule, message):
    if isinstance(rule, list):
        for y in rule:
            yield from sequence(y, message)
    else:
        if message and message[0] == rule:
            yield message[1:]

def sequence(x, message):
    if x:
        for y in parse(rules[x[0]], message):
            yield from sequence(x[1:], y)
    else:
        yield message

print(sum(any(m == '' for m in parse(rules['0'], x)) for x in messages))
