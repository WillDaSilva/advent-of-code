with open('01.in') as f:
    data = sorted([int(x) for x in f])

for x in reversed(data):
    for y in data:
        if x + y > 2020:
            break
        elif x + y == 2020:
            print(x * y)
            exit()

