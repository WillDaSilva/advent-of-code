with open('03.in') as f:
    grid = [[x == '#' for x in y.strip()] for y in f]

x, y = 0, 0
dx, dy = 3, 1
hit = 0
while y < len(grid):
    hit += grid[y][x]
    x = (x + dx) % len(grid[0])
    y += dy

print(hit)
