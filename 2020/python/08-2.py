with open('08.in') as f:
    instructions = [(a, int(b)) for a, b in [x.strip().split() for x in f]]

def run(alter_idx):
    accumulator = 0
    idx = 0
    seen = set()
    terminated = False
    while idx not in seen:
        if idx == len(instructions):
            terminated = True
            break
        seen.add(idx)
        op, val = instructions[idx]
        if idx == alter_idx:
            op = 'jmp' if op == 'nop' else 'nop'
        if op == 'jmp':
            idx += val
            continue
        elif op == 'acc':
            accumulator += val
        idx += 1
    return terminated * accumulator

print(sum(run(x) for x in range(len(instructions)) if instructions[x][0] != 'acc'))
