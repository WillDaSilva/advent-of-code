from collections import defaultdict

with open('21.in') as f:
    foods = [x[:-1].split(' (contains ') for x in f.read().splitlines()]
    foods = [(set(a.split()), b.split(', ')) for a, b in foods]

possible_sources = defaultdict(lambda: ingredients.copy())
for ingredients, allergens in foods:
    for allergen in allergens:
        possible_sources[allergen] &= ingredients

while not all(len(x) == 1 for x in possible_sources.values()):
    for allergen, ingredients in possible_sources.items():
        if len(ingredients) == 1:
            for v in (v for k, v in possible_sources.items() if k != allergen):
                v.discard(next(iter(ingredients)))
print(','.join(next(iter(possible_sources[x])) for x in sorted(possible_sources)))
