from collections import deque

with open('22.in') as f:
    p1, p2 = (deque(int(y) for y in x.splitlines()[1:]) for x in f.read().split('\n\n'))

while p1 and p2:
    c1, c2 = p1.popleft(), p2.popleft()
    if c1 > c2:
        p1.extend((c1, c2))
    else:
        p2.extend((c2, c1))
winner = p1 if p1 else p2
print(sum(i * x for i, x in enumerate(reversed(winner), start=1)))
