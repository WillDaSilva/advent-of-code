from collections import Counter

valid = 0
with open('02.in') as f:
    for x, y in (x.split(': ') for x in f):
        a, b = x.split(' ')
        q, p = a.split('-')
        c = Counter(y)
        if c[b] >= int(q) and c[b] <= int(p):
            valid += 1
print(valid)


