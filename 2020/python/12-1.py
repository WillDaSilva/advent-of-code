directions = ((-1, 0), (0, 1), (1, 0), (0, -1)) # (E, N, W, S)
actions = {
    'N': lambda x, y, d, m: (x, y + m, d),
    'S': lambda x, y, d, m: (x, y - m, d),
    'E': lambda x, y, d, m: (x - m, y, d),
    'W': lambda x, y, d, m: (x + m, y, d),
    'L': lambda x, y, d, m: (x, y, (d + m // 90) % 4),
    'R': lambda x, y, d, m: (x, y, (d - m // 90) % 4),
    'F': lambda x, y, d, m: (x + directions[d][0] * m, y + directions[d][1] * m, d),
}

with open('12.in') as f:
    instructions = ((actions[x[0]], int(x[1:])) for x in f.read().splitlines())

state = (0, 0, 0) # [x, y, direction index]
for action, magnitude in instructions:
    state = action(*state, magnitude)
print(abs(state[0]) + abs(state[1]))
