d = {
    'e': (1, -1, 0),
    'w': (-1, 1, 0),
    'ne': (1, 0, -1),
    'nw': (0, 1, -1),
    'se': (0, -1, 1),
    'sw': (-1, 0, 1)
}

def split(s):
    i = 0
    while i < len(s):
        if s[i] in ('e', 'w'):
            yield s[i]
            i += 1
        else:
            yield s[i:i+2]
            i += 2

with open('24.in') as f:
    tiles = [[d[y] for y in split(x)] for x in f.read().splitlines()]

flipped = set()
for tile in tiles:
    flipped ^= {tuple(sum(a) for a in zip(*tile))}
print(len(flipped))
