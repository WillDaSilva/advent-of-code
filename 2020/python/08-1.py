with open('08.in') as f:
    instructions = [(a, int(b)) for a, b in [x.strip().split() for x in f]]

accumulator = 0
idx = 0
seen = set()
while idx not in seen:
    seen.add(idx)
    op, val = instructions[idx]
    if op == 'jmp':
        idx += val
        continue
    elif op == 'acc':
        accumulator += val
    idx += 1
print(accumulator)