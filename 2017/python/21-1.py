import itertools as it
import numpy as np

with open('21.in', 'r') as puzzleInputFile:
    rules = puzzleInputFile.readlines()

def D4Symmetries(grid):
    yield grid          # e
    for k in range(4):
        grid = np.rot90(grid)
        yield grid      # R^k
    grid = np.flipud(grid)
    yield grid          # T
    for k in range(4):
        grid = np.rot90(grid)
        yield grid      # TR^k

art = np.asarray([[False,True,False],[False,False,True],[True,True,True]])
states = {'.': False, '#': True}
for i in range(len(rules)):
    rules[i] = [x.strip().split('/') for x in rules[i].split('=>')]
    rules[i] = [[[states[z] for z in y] for y in x] for x in rules[i]]
a = it.chain.from_iterable((D4Symmetries(np.asarray(x[0])) for x in rules))
b = it.chain.from_iterable((it.repeat(np.asarray(x[1]), 10) for x in rules))
rules = {tuple(a.flatten()): b for a, b in zip(a, b)}

for i in range(5):
    x, y = art.shape
    z = (2, 3)[len(art) % 2]
    art = art.reshape(x//z, z, -1, z).swapaxes(1, 2).reshape(-1, y//z, z, z)
    art = np.block([[rules[tuple(x.flatten())] for x in y] for y in art])
print(np.sum(art))
