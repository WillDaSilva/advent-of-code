with open('07.in', 'r') as puzzleInputFile:
    programs = puzzleInputFile.readlines()

for i in range(len(programs)):
    programs[i] = programs[i].split('->')
    programs[i][0] = programs[i][0].split()
    programs[i][0] = (programs[i][0][0], programs[i][0][1].strip('()'))
    if len(programs[i]) == 2:
        programs[i][1] = programs[i][1].split(', ')
        programs[i][1] = tuple(map(lambda x: x.strip(' \n'), programs[i][1]))
    programs[i] = tuple(programs[i])
nameToNode = dict((x[0][0], x) for x in programs)

hasParent = set()
for program in programs:
    if len(program) == 2:
        for child in program[1]:
            hasParent.add(child)
root = nameToNode[next(iter(set(x[0][0] for x in programs) - hasParent))]

def weight(node):
    if len(node) != 2: return int(node[0][1])
    return int(node[0][1]) + sum(weight(nameToNode[child]) for child in node[1])

nodeToWeight = dict()
for node in programs:
    nodeToWeight[node] = nodeToWeight.get(node, weight(node))

q = [root]
while len(q):
    node = q.pop()
    if len(node) == 1: continue
    children = [nameToNode[x] for x in node[1]]
    if len(children) in (0, 1): continue
    q.extend(children)
    childrenWeights = [nodeToWeight[child] for child in children]
    if not all(x == childrenWeights[0] for x in childrenWeights):
        for i, childWeight in enumerate(childrenWeights):
            if childrenWeights.count(childWeight) == 1:
                problemNode = children[i]
                goodNodeWeight = nodeToWeight[children[(i + 1) % len(children)]]
                difference = childWeight - goodNodeWeight
                correctedWeight = int(problemNode[0][1]) - difference
print(correctedWeight)
