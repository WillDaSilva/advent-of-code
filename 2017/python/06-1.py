with open('06.in', 'r') as puzzleInputFile:
    puzzleInput = list(map(int, puzzleInputFile.read().split()))

def distribute(targetIndex):
    store = puzzleInput[targetIndex]
    puzzleInput[targetIndex] = 0
    while store:
        targetIndex += 1
        puzzleInput[targetIndex % len(puzzleInput)] += 1
        store -= 1

seen = set()
while True:
    blocks = tuple(puzzleInput)
    if blocks in seen:
        break
    seen.add(blocks)
    targetIndex = blocks.index(max(blocks))
    distribute(targetIndex)
print(len(seen))
