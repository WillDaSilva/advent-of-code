from collections import deque

with open('16.in', 'r') as puzzleInputFile:
    moves = puzzleInputFile.read().strip().split(',')

programs = deque(map(lambda x: chr(ord('a') + x), range(16)))

def spin(x):
    programs.rotate(int(x))

def exchange(x):
    a, b = map(int, x.split('/'))
    programs[a], programs[b] = programs[b], programs[a]

def partner(x):
    a, b = map(programs.index, x.split('/'))
    programs[a], programs[b] = programs[b], programs[a]

def doMove(move):
    {
        's': spin,
        'x': exchange,
        'p': partner
    }.get(move[0])(move[1:])

for move in moves: doMove(move)
print(''.join(programs))
