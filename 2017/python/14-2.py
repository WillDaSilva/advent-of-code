knotHash = getattr(__import__('10-2', fromlist=('knotHash')), 'knotHash')

with open('14.in', 'r') as puzzleInputFile:
    puzzleInput = puzzleInputFile.read().strip()

def adjacents(x, y):
    a = [[x, y]]*4
    b = ((1, 0), (0, 1), (-1, 0), (0, -1))
    return (tuple(sum(pair) for pair in zip(*pairs)) for pairs in zip(a, b))

rows = map(knotHash, (f'{puzzleInput}-{x}' for x in range(128)))
rows = map(lambda x: bin(int(x, 16))[2:].zfill(128), rows)
grid = [[int(bit) for bit in row] for row in rows]

def clearRegion(x, y):
    stack = set()
    stack.add((x, y))
    while stack:
        x, y = stack.pop()
        for ax, ay in adjacents(x, y):
            if ax >= 0 and ax < len(grid[0]) and ay >= 0 and ay < len(grid):
                if grid[ay][ax]:
                    stack.add((ax, ay))
        grid[y][x] = 0

numRegions = 0
for y, row in enumerate(grid):
    for x, bit in enumerate(row):
        if bit:
            numRegions += 1
            clearRegion(x, y)
print(numRegions)
