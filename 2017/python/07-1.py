with open('07.in', 'r') as puzzleInputFile:
    programs = puzzleInputFile.readlines()

for i in range(len(programs)):
    programs[i] = programs[i].split('->')
    programs[i][0] = programs[i][0].split()
    programs[i][0][1] = programs[i][0][1].strip('()')
    if len(programs[i]) == 2:
        programs[i][1] = programs[i][1].split(', ')
        programs[i][1] = tuple(map(lambda x: x.strip(' \n'), programs[i][1]))

hasParent = set()
for program in programs:
    if len(program) == 2:
        for child in program[1]:
            hasParent.add(child)
print(str(set(x[0][0] for x in programs) - hasParent).strip('\'{}'))
