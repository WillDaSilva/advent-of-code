with open('02.in', 'r') as puzzleInputFile:
    puzzleInput = puzzleInputFile.readlines()

def largestDifference(row):
    largestValue = 0
    smallestValue = 10000
    for value in row:
        if value > largestValue: largestValue = value
        if value < smallestValue: smallestValue = value
    return largestValue - smallestValue

print(sum(largestDifference(map(int, line.split())) for line in puzzleInput))
