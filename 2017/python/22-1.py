from collections import defaultdict
import itertools as it

with open('22.in', 'r') as puzzleInputFile:
    puzzleInput = puzzleInputFile.readlines()

states = {'.': False, '#': True}
puzzleInputCoordinates = it.product(range(len(puzzleInput)), repeat=2)
grid = {(x, y): states[puzzleInput[y][x]] for x, y in puzzleInputCoordinates}
grid = defaultdict(bool, grid)

x = y = len(puzzleInput) // 2
directions = ((0, -1), (1, 0), (0, 1), (-1, 0)) # up, right, down, left
direction = 0
infectionsCaused = 0
for burst in range(10000):
    isNodeInfected = grid[(x, y)]
    direction = (direction + (-1, 1)[isNodeInfected]) % 4 # turn
    infectionsCaused += not isNodeInfected
    grid[(x, y)] = not isNodeInfected # toggle infection at (x, y)
    x, y = (sum(z) for z in zip((x, y), directions[direction]))
print(infectionsCaused)
