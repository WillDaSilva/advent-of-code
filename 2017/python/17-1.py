from collections import deque

with open('17.in', 'r') as puzzleInputFile:
    steps = int(puzzleInputFile.read().strip())

circularBuffer = deque()
for i in range(2018):
    circularBuffer.rotate(-steps)
    circularBuffer.append(i)
print(circularBuffer[0])
