import itertools as it
from math import sqrt

def isPrime(n):
    return n > 1 and all(n%i for i in it.islice(it.count(2), int(sqrt(n)-1)))

# Optimising assembly is easy and fun, just remember: rise over run
print(sum(not isPrime(79 * 100 + 100000 + 17 * x) for x in range(1001)))
