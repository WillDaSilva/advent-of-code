with open('24.in', 'r') as puzzleInputFile:
    components = puzzleInputFile.readlines()

components = [tuple(int(y) for y in x.strip().split('/')) for x in components]

def buildBridge(bridge=[(0, 0)], port=0):
    port = bridge[-1][0 if bridge[-1][0] != port else 1]
    for component in [x for x in components if port in x and x not in bridge]:
        yield from buildBridge(bridge + [component], port)
    yield bridge

print(max(map(lambda bridge: sum(y for x in bridge for y in x), buildBridge())))
