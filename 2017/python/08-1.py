from collections import defaultdict
from itertools import chain
import operator

with open('08.in', 'r') as puzzleInputFile:
    puzzleInput = puzzleInputFile.readlines()

ops = {
    'inc': operator.add,
    'dec': operator.sub,
    '==': operator.eq,
    '!=': operator.ne,
    '>': operator.gt,
    '>=': operator.ge,
    '<': operator.lt,
    '<=': operator.le
}

def triofy(item):
    register, operation, integer = item.split()
    return (register, ops[operation], int(integer))

operations, conditions = zip(*[x.split(' if ') for x in puzzleInput])
trios = list(map(triofy, chain(operations, conditions)))
operations, conditions = trios[:len(trios)//2], trios[len(trios)//2:]

registers = defaultdict(int)
for i in range(len(operations)):
    if conditions[i][1](registers[conditions[i][0]], conditions[i][2]):
        result = operations[i][1](registers[operations[i][0]], operations[i][2])
        registers[operations[i][0]] = result

print(max(registers.values()))
