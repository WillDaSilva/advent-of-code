with open('05.in', 'r') as puzzleInputFile:
    maze = list(map(int, puzzleInputFile.read().split()))

numJumps = 0
targetIndex = 0

while targetIndex < len(maze):
    maze[targetIndex] += 1
    targetIndex = targetIndex + maze[targetIndex] - 1
    numJumps += 1

print(numJumps)
