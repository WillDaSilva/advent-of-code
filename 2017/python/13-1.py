with open('13.in', 'r') as puzzleInputFile:
    puzzleInput = [x.strip().split(': ') for x in puzzleInputFile.readlines()]

firewall = {int(x): int(y) for x, y in puzzleInput}

def scannerPosition(numBlocks, tick):
    offset = tick % ((numBlocks - 1) * 2)
    return offset if offset <= numBlocks else (numBlocks - 1) * 2 - offset

def detected(layer):
    return scannerPosition(firewall[layer], layer) == 0

print(sum(layer * firewall[layer] for layer in filter(detected, firewall)))
