pipes = []

with open('12.in', 'r') as puzzleInputFile:
    for line in puzzleInputFile:
        pipes.append(tuple(map(int, line.split(' <-> ')[1].split(', '))))

stack = [0]
connected = set()
while stack:
    pids = [x for x in pipes[stack.pop()] if x not in connected]
    connected.update(pids)
    stack.extend(pids)
print(len(connected))
