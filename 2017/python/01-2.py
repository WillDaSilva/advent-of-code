with open('01.in', 'r') as puzzleInputFile:
    puzzleInput = puzzleInputFile.read()

total = 0
for i in range(len(puzzleInput)):
    index = (i + len(puzzleInput) // 2) % len(puzzleInput)
    if puzzleInput[i] == puzzleInput[index]:
        total += int(puzzleInput[i])
print(total)
