def characterStream(filename):
    with open(filename, 'r') as fileStreamFile:
        for line in fileStreamFile:
            for character in line:
                yield character

def countGarbage(stream):
    garbage = 0
    for c in stream:
        if c == '!':
            next(stream)
        elif c == '>':
            return garbage
        else:
            garbage += 1

stream = characterStream('09.in')
garbage = 0
for c in stream:
    if c == '!':
        next(stream)
    elif c == '<':
        garbage += countGarbage(stream)

print(garbage)
