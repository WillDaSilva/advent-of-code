import itertools as it

with open('04.in', 'r') as puzzleInputFile:
    puzzleInput = puzzleInputFile.readlines()

numValid = 0
for passphrase in puzzleInput:
    words = passphrase.split()
    if (len(set(words)) == len(words)):
        numValid += len({tuple(sorted(word)) for word in words}) == len(words)
print(numValid)
