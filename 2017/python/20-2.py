with open('20.in', 'r') as puzzleInputFile:
    particles = puzzleInputFile.readlines()

for i in range(len(particles)):
    particles[i] = map(lambda x: x[3:-1], particles[i].strip().split(', '))
    particles[i] = map(lambda x: list(map(int, x.split(','))), particles[i])
    particles[i] = tuple(particles[i])

remove = set()
for i in range(100):
    particles = [x for x in particles if tuple(x[0]) not in remove]
    spaceOccupied = set()
    for particle in particles:
        for c in range(3): particle[1][c] += particle[2][c]
        for c in range(3): particle[0][c] += particle[1][c]
        pos = tuple(particle[0])
        if pos in spaceOccupied: remove.add(pos)
        else: spaceOccupied.add(tuple(particle[0]))

print(len(particles))
