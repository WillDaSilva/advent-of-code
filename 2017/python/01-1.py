with open('01.in', 'r') as puzzleInputFile:
    puzzleInput = puzzleInputFile.read()

total = 0
for i in range(len(puzzleInput)):
    if puzzleInput[i] == puzzleInput[(i + 1) % len(puzzleInput)]:
        total += int(puzzleInput[i])
print(total)
