from collections import defaultdict
from queue import Queue
from threading import Thread

with open('18.in', 'r') as puzzleInputFile:
    instructions = [x.split() for x in puzzleInputFile.readlines()]

def retriveValue(x, registers):
    try:
        return int(x)
    except ValueError:
        return registers[x]

def dance(programID, send, receive):
    registers = defaultdict(int)
    registers['p'] = programID
    index = 0
    while index < len(instructions):
        if len(instructions[index]) == 2:
            operation, register = instructions[index]
        else:
            operation, register, x = instructions[index]
            value = retriveValue(x, registers)
        if   operation == 'set': registers[register] = value
        elif operation == 'add': registers[register] += value
        elif operation == 'mul': registers[register] *= value
        elif operation == 'mod': registers[register] %= value
        elif operation == 'snd':
            sendCount[programID] += 1
            send.put(retriveValue(register, registers))
        elif operation == 'rcv':
             received = receive.get()
             if received is None: break
             registers[register] = received
        elif operation == 'jgz' and retriveValue(register, registers) > 0:
                index += value - 1
        index += 1

sendCount = defaultdict(int)
aQueue, bQueue = Queue(), Queue()
a = Thread(target=dance, args=(0, aQueue, bQueue))
b = Thread(target=dance, args=(1, bQueue, aQueue))
a.start(); b.start()
while True:
    if aQueue.empty() and bQueue.empty():
        aQueue.put(None); bQueue.put(None)
        break
a.join(); b.join()
print(sendCount[1])
