with open('02.in', 'r') as puzzleInputFile:
    puzzleInput = puzzleInputFile.readlines()

def evenlyDivisible(row):
    row = list(sorted(row))
    for a in range(len(row)):
        for b in row[a + 1:]:
            if b % row[a] == 0:
                return b // row[a]
    raise Exception(f'No pair of divisible numbers found in {row}')

print(sum(evenlyDivisible(map(int, line.split())) for line in puzzleInput))
