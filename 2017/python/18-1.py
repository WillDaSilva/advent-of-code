from collections import defaultdict

with open('18.in', 'r') as puzzleInputFile:
    instructions = [x.split() for x in puzzleInputFile.readlines()]

def retriveValue(x):
    try:
        return int(x)
    except ValueError:
        return registers[x]

lastPlayed = None
registers = defaultdict(int)
index = 0
while index < len(instructions):
    if len(instructions[index]) == 2:
        operation, register = instructions[index]
    else:
        operation, register, x = instructions[index]
        value = retriveValue(x)
    if   operation == 'set': registers[register] = value
    elif operation == 'add': registers[register] += value
    elif operation == 'mul': registers[register] *= value
    elif operation == 'mod': registers[register] %= value
    elif operation == 'snd': lastPlayed = registers[register]
    elif operation == 'rcv' and registers[register] != 0: break
    elif operation == 'jgz' and registers[register] > 0: index += value - 1
    index += 1
print(lastPlayed)
