with open('23.in', 'r') as puzzleInputFile:
    instructions = [x.split() for x in puzzleInputFile.readlines()]

def retriveValue(x):
    try:
        return int(x)
    except ValueError:
        return registers[x]

mulCount = 0
registers = {x: 0 for x in 'abcdefgh'}
index = 0
while index < len(instructions):
    if len(instructions[index]) == 2:
        operation, register = instructions[index]
    else:
        operation, register, x = instructions[index]
        value = retriveValue(x)
    if operation == 'set':
        registers[register] = value
    elif operation == 'sub':
        registers[register] -= value
    elif operation == 'mul':
        registers[register] *= value
        mulCount += 1
    elif operation == 'jnz' and retriveValue(register) != 0:
        index += value - 1
    index += 1
print(mulCount)
