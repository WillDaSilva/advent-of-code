with open('03.in', 'r') as puzzleInputFile:
    puzzleInput = int(puzzleInputFile.read())

def adjacents(x, y):
    a = [[x, y]]*8
    b = ((1, 0), (1, 1), (0, 1), (-1, 1), (-1, 0), (-1, -1), (0, -1), (1, -1))
    return (tuple(sum(pair) for pair in zip(*pairs)) for pairs in zip(a, b))

d = {(0, 0): 1}

x, y = 1, 0
dx, dy = 1, 0
while True:
    if (x < 0 and x == -y) or (x > 0 and x == 1 - y) or x == y:
        dx, dy = -dy, dx # turn
    valueAtXY = sum(d.get(coordinate, 0) for coordinate in adjacents(x, y))
    if valueAtXY > puzzleInput:
        print(valueAtXY)
        break
    else:
        d[(x, y)] = valueAtXY
    x, y = x + dx, y + dy # move
