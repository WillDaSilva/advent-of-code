with open('17.in', 'r') as puzzleInputFile:
    steps = int(puzzleInputFile.read().strip())

index = 0
for i in range(1, 50000001):
    index = (index + steps) % i + 1
    if index == 1:
        n = i
print(n)
