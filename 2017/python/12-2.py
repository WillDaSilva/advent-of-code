pipes = []

with open('12.in', 'r') as puzzleInputFile:
    for line in puzzleInputFile:
        pipes.append(tuple(map(int, line.split(' <-> ')[1].split(', '))))

numGroups = 0
programs = set(range(len(pipes)))
while programs:
    stack = [programs.pop()]
    connected = set()
    while stack:
        pids = [x for x in pipes[stack.pop()] if x not in connected]
        connected.update(pids)
        stack.extend(pids)
    programs -= connected
    numGroups += 1
print(numGroups)
