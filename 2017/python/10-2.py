from collections import deque
from functools import reduce
from itertools import islice
from operator import xor

class sliceable_deque(deque):
    def __getitem__(self, index):
        if isinstance(index, slice):
            return type(self)(islice(self, index.start, index.stop, index.step))
        return deque.__getitem__(self, index)
    def __setitem__(self, index, value):
        if isinstance(index, slice):
            # Does not have the same behaviour as standard list assignment!
            value = tuple(value)
            start = index.start or 0
            stop = min(index.stop or float('inf'), len(value) - 1) + 1
            step = index.step or 1
            for i in range(start, stop, step):
                deque.__setitem__(self, i, value[i-start])
        else:
            deque.__setitem__(self, index, value)

def knotHash(data):
    lengths = [ord(x) for x in data]
    lengths.extend([17, 31, 73, 47, 23])
    knot = sliceable_deque(range(256))
    position = 0
    skip = 0
    for i in range(64):
        for length in lengths:
            knot.rotate(-position)
            knot[:length] = reversed(knot[:length])
            knot.rotate(position)
            position += length + skip
            skip += 1
    denseHash = map(lambda x: reduce(xor, x), zip(*[iter(knot)]*16))
    return ''.join(f'{x:02x}' for x in denseHash)

if __name__ == '__main__':
    with open('10.in', 'r') as puzzleInputFile:
        puzzleInput = puzzleInputFile.read().strip()
    print(knotHash(puzzleInput))
