with open('04.in', 'r') as puzzleInputFile:
    puzzleInput = puzzleInputFile.readlines()

numValid = 0
for passphrase in puzzleInput:
    words = passphrase.split()
    numValid += len(set(words)) == len(words)
print(numValid)
