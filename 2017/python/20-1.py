with open('20.in', 'r') as puzzleInputFile:
    particles = puzzleInputFile.readlines()

for i in range(len(particles)):
    particles[i] = map(lambda x: x[3:-1], particles[i].strip().split(', '))
    particles[i] = map(lambda x: list(map(int, x.split(','))), particles[i])

# avp provides the magnitudes of the acceleration, velocity, and position
avp = lambda x: tuple(reversed(tuple(map(lambda x: sum(map(abs, x)), x))))
print(min(enumerate(particles), key=lambda x: avp(x[1]))[0])
