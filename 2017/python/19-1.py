with open('19.in', 'r') as puzzleInputFile:
    routes = [row for row in puzzleInputFile]

seen = []
x, y = routes[0].index('|'), 0
dx, dy = 0, 1
while routes[y][x] != ' ':
    if routes[y][x] == '+':
        if dx and routes[y][x + dx] == ' ':
            dx, dy = 0, (-1, 1)[routes[y + 1][x] != ' ']
        elif dy and routes[y + dy][x] == ' ':
            dx, dy = (-1, 1)[routes[y][x + 1] != ' '], 0
    elif routes[y][x].isalpha():
        seen.append(routes[y][x])
    x += dx
    y += dy
print(''.join(seen))