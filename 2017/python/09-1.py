def characterStream(filename):
    with open(filename, 'r') as fileStreamFile:
        for line in fileStreamFile:
            for character in line:
                yield character

def movePastGarbage(stream):
    for c in stream:
        if c == '!':
            next(stream)
        elif c == '>':
            return

stream = characterStream('09.in')
stack = 0
score = 0
for c in stream:
    if c == '!':
        next(stream)
    elif c == '<':
        movePastGarbage(stream)
    elif c == '{':
        stack += 1
    elif c == '}':
        score += stack
        stack -= 1

print(score)
