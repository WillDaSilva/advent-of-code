with open('06.in', 'r') as puzzleInputFile:
    puzzleInput = list(map(int, puzzleInputFile.read().split()))

def distribute(targetIndex):
    store = puzzleInput[targetIndex]
    puzzleInput[targetIndex] = 0
    while store:
        targetIndex += 1
        puzzleInput[targetIndex % len(puzzleInput)] += 1
        store -= 1

seen = set()
enteredLoop = False
loopSize = 0
while True:
    blocks = tuple(puzzleInput)
    if not enteredLoop:
        if blocks in seen:
            startOfLoop = blocks
            enteredLoop = True
        else:
            seen.add(blocks)
    else:
        loopSize += 1
        if blocks == startOfLoop:
            break
    targetIndex = blocks.index(max(blocks))
    distribute(targetIndex)
print(loopSize)
