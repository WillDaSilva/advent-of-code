from collections import deque
from itertools import islice

class sliceable_deque(deque):
    def __getitem__(self, index):
        if isinstance(index, slice):
            return type(self)(islice(self, index.start, index.stop, index.step))
        return deque.__getitem__(self, index)
    def __setitem__(self, index, value):
        if isinstance(index, slice):
            # Does not have the same behaviour as standard list assignment!
            value = tuple(value)
            start = index.start or 0
            stop = min(index.stop or float('inf'), len(value) - 1) + 1
            step = index.step or 1
            for i in range(start, stop, step):
                deque.__setitem__(self, i, value[i-start])
        else:
            deque.__setitem__(self, index, value)

with open('10.in', 'r') as puzzleInputFile:
    lengths = map(int, puzzleInputFile.read().split(','))

knot = sliceable_deque(range(256))
position = 0
skip = 0
for length in lengths:
    knot.rotate(-position)
    knot[:length] = reversed(knot[:length])
    knot.rotate(position)
    position += length + skip
    skip += 1

print(knot[0] * knot[1])
