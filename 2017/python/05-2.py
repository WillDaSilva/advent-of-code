with open('05.in', 'r') as puzzleInputFile:
    maze = list(map(int, puzzleInputFile.read().split()))

numJumps = 0
targetIndex = 0

while targetIndex < len(maze):
    alteration = (1, -1)[maze[targetIndex] >= 3]
    maze[targetIndex] += alteration
    targetIndex = targetIndex + maze[targetIndex] - alteration
    numJumps += 1

print(numJumps)
