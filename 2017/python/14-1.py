knotHash = getattr(__import__('10-2', fromlist=('knotHash')), 'knotHash')

with open('14.in', 'r') as puzzleInputFile:
    puzzleInput = puzzleInputFile.read().strip()

rows = map(knotHash, (f'{puzzleInput}-{x}' for x in range(128)))
rows = map(lambda x: bin(int(x, 16))[2:].zfill(128), rows)
print(sum(x.count('1') for x in rows))
