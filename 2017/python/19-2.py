with open('19.in', 'r') as puzzleInputFile:
    routes = [row for row in puzzleInputFile]

steps = 0
x, y = routes[0].index('|'), 0
dx, dy = 0, 1
while routes[y][x] != ' ':
    if routes[y][x] == '+':
        if dx and routes[y][x + dx] == ' ':
            dx, dy = 0, (-1, 1)[routes[y + 1][x] != ' ']
        elif dy and routes[y + dy][x] == ' ':
            dx, dy = (-1, 1)[routes[y][x + 1] != ' '], 0
    x += dx
    y += dy
    steps += 1
print(steps)