with open('15.in', 'r') as puzzleInputFile:
    puzzleInput = puzzleInputFile.readlines()

def generator(value, factor):
    while True:
        value = (value * factor) % 2147483647
        yield value

generators = (
    generator(int(''.join(x for x in puzzleInput[0] if x.isdigit())), 16807),
    generator(int(''.join(x for x in puzzleInput[1] if x.isdigit())), 48271)
)

matches = 0
for i in range(40000000):
    a, b = next(zip(*generators))
    matches += bin(a)[2:][-16:].zfill(16) == bin(b)[2:][-16:].zfill(16)
print(matches)
