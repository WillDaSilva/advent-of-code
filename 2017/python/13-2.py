with open('13.in', 'r') as puzzleInputFile:
    puzzleInput = [x.strip().split(': ') for x in puzzleInputFile.readlines()]

firewall = {int(x): int(y) for x, y in puzzleInput}

def scannerPosition(numBlocks, tick):
    offset = tick % ((numBlocks - 1) * 2)
    return offset if offset <= numBlocks else (numBlocks - 1) * 2 - offset

def detected(layer, delay):
    return scannerPosition(firewall[layer], layer + delay) == 0

delay = 0
while any(detected(layer, delay) for layer in firewall): delay += 1
print(delay)
