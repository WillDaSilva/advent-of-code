directions = dict(zip(('n', 'ne', 'se', 's', 'sw', 'nw'), range(6)))

with open('11.in', 'r') as puzzleInputFile:
    steps = [directions[x] for x in puzzleInputFile.read().strip().split(',')]

# (n, ne, se, s, sw, nw)
offset = ((0, -1), (1, -1), (1, 0), (0, 1), (-1, 1), (-1, 0))

def distance(x1, y1, x2, y2):
    return max(abs(x) for x in (x1 - x2, y1 - y2, -(x1 + y1) + x2 + y2))

maxDistance = 0
x, y = 0, 0
for step in steps:
    dx, dy = offset[step]
    x += dx
    y += dy
    currentDistance = distance(x, y, 0, 0)
    if currentDistance > maxDistance: maxDistance = currentDistance

print(maxDistance)
