from collections import deque
import re

with open('25.in', 'r') as puzzleInputFile:
    puzzleInput = re.split(r'^\n', puzzleInputFile.read(), flags=re.M)

puzzleInput = [x.strip().split('\n') for x in puzzleInput]
diagnosticStep = int(puzzleInput[0][1].split()[-2])
puzzleInput = [[y.split()[-1][0] for y in x] for x in puzzleInput[1:]]
states = {}
for state in puzzleInput:
    ifZero = (int(state[2]), {'r': 1, 'l': -1}[state[3]], state[4])
    ifOne = (int(state[6]), {'r': 1, 'l': -1}[state[7]], state[8])
    states[state[0]] = (ifZero, ifOne)
tape = deque([0]*10000)
state = states['A']
for step in range(diagnosticStep):
    x = tape[0]
    tape[0] = state[x][0]
    tape.rotate(-state[x][1])
    state = states[state[x][2]]
print(sum(tape))
