with open('03.in', 'r') as puzzleInputFile:
    puzzleInput = int(puzzleInputFile.read())

def spiralGenerator():
    x = y = 0
    dx, dy = 0, -1
    while True:
        if (x < 0 and x == -y) or (x > 0 and x == 1 - y) or x == y:
            dx, dy = -dy, dx # turn
        yield (x, y)
        x, y = x + dx, y + dy # move

spiral = spiralGenerator()
[next(spiral) for _ in range(puzzleInput -1)]
print(sum(map(abs, next(spiral))))
