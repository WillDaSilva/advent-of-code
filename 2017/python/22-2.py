from collections import defaultdict
import itertools as it

with open('22.in', 'r') as puzzleInputFile:
    puzzleInput = puzzleInputFile.readlines()

states = {'.': 0, '#': 2}
puzzleInputCoordinates = it.product(range(len(puzzleInput)), repeat=2)
grid = {(x, y): states[puzzleInput[y][x]] for x, y in puzzleInputCoordinates}
grid = defaultdict(int, grid)

x = y = len(puzzleInput) // 2
directions = ((0, -1), (1, 0), (0, 1), (-1, 0)) # up, right, down, left
direction = 0
infectionsCaused = 0
for burst in range(10000000):
    nodeState = grid[(x, y)]
    direction = {
        0: (direction - 1) % 4, # turn left
        1: direction,           # do not turn
        2: (direction + 1) % 4, # turn right
        3: (direction + 2) % 4  # reverse direction
    }[nodeState]
    infectionsCaused += nodeState == 1
    grid[(x, y)] = (nodeState + 1) % 4
    x, y = (sum(z) for z in zip((x, y), directions[direction]))
print(infectionsCaused)
