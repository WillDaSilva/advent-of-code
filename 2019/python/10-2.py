from math import atan2, gcd, pi

with open('10.in') as f:
    asteroids = [[y == '#' for y in x.strip()] for x in f.readlines()]
h = len(asteroids)
w = len(asteroids[0])

def detectable(y1, x1):
    detected = set()
    for y2 in range(h):
        for x2 in range(w):
            if asteroids[y2][x2] and (y1 != y2 or x1 != x2):
                dx = x2 - x1
                dy = y2 - y1
                g = abs(gcd(dy, dx))
                detected.add((-dy // g, dx // g))
    return detected

r = {(y, x):detectable(y, x) for y in range(h) for x in range(w) if asteroids[y][x]}
(y, x), detected = max(r.items(), key=lambda x: len(x[1]))
key = lambda x: z if (z := atan2(x[0], x[1])) <= pi/2.0 else z - 2*pi
dy, dx = list(reversed(sorted(detected, key=key)))[199]
print(100 * (x + dx) + y - dy)
