from collections import defaultdict
import itertools as it
import time

with open('13.in') as f:
    program = [int(x) for x in f.readline().split(',')]

def digitize(n):
    while n:
        n, x = divmod(n, 10)
        yield x

def intcode(program):
    program = defaultdict(int, {i:x for i, x in enumerate(program)})
    valences = {1: 3, 2: 3, 3: 1, 4: 1, 5: 2, 6: 2, 7: 3, 8: 3, 9: 1}
    position = 0
    base = 0
    while program[position] != 99:
        digits = list(digitize(program[position]))
        opcode = 10 * (digits[1] if len(digits) > 1 else 0) + digits[0]
        valence = valences[opcode]
        modes = digits[2:] + [0] * (valence - len(digits) + 2)
        parameters = []
        indices = []
        for x in range(1, valence + 1):
            if modes[x-1] == 1:
                indices.append(None)
                parameters.append(program[position + x])
            elif modes[x-1] == 2:
                indices.append(program[position + x] + base)
                parameters.append(program[program[position + x] + base])
            else:
                indices.append(program[position + x])
                parameters.append(program[program[position + x]])
        position += valence + 1
        if opcode == 1:
            program[indices[2]] = parameters[0] + parameters[1]
        elif opcode == 2:
            program[indices[2]] = parameters[0] * parameters[1]
        elif opcode == 3:
            program[indices[0]] = yield
        elif opcode == 4:
            yield parameters[0]
        elif opcode == 5:
            if parameters[0]: position = parameters[1]
        elif opcode == 6:
            if parameters[0] == 0: position = parameters[1]
        elif opcode == 7:
            program[indices[2]] = int(parameters[0] < parameters[1])
        elif opcode == 8:
            program[indices[2]] = int(parameters[0] == parameters[1])
        elif opcode == 9:
            base += parameters[0]
    raise StopIteration()

program[0] = 2
breakout = intcode(program)
ball = 0
paddle = 0
grid = defaultdict(int)
try:
    for i in it.count():
        a = next(breakout)
        if a is None: # input is needed
            if ball == paddle:
                a = breakout.send(0)
            elif ball > paddle:
                a = breakout.send(1)
            else:
                a = breakout.send(-1)
        b = next(breakout)
        c = next(breakout)
        if a == -1 and b == 0:
            score = c
        else:
            if c == 3:
                paddle = a
            elif c == 4:
                ball = a
            grid[(a, b)] = c
        if i > 1000:
            min_x = min(x for x, _ in grid)
            max_x = max(x for x, _ in grid)
            min_y = min(y for _, y in grid)
            max_y = max(y for _, y in grid)
            for y in range(min_y, max_y + 1):
                print(''.join(' █#_*'[grid[(x, y)]] for x in range(min_x, max_x + 1)))
            time.sleep(0.01)
except StopIteration:
    pass
print(score)

