import itertools as it
from operator import sub

with open('16.in') as f:
    signal = f.readline().strip()

signal = ([int(x) for x in signal]*10000)[int(signal[:7]):]

for _ in range(100):
    signal = [abs(x) % 10 for x in it.accumulate(signal, sub, initial=sum(signal))]

print(''.join(map(str, signal[:8])))
