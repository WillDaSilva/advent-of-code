with open('01.in') as f:
    data = (int(x) for x in f.readlines())

print(sum(x//3 - 2 for x in data))
