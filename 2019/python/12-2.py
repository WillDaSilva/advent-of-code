from dataclasses import dataclass
from functools import reduce
import itertools as it
from math import gcd
import re

@dataclass
class Moon:
    position: list
    velocity: list

with open('12.in') as f:
    moons = tuple(Moon([int(y) for y in re.findall(r'-?\d+', x)], [0]*3) for x in f.readlines())

def lcm(a, b):
    return abs(a*b) // gcd(a, b)

seen = [set(), set(), set()]
axes = [0, 1, 2]
for i in it.count():
    if not axes:
        break
    for axis in axes:
        state = tuple((moon.position[axis], moon.velocity[axis]) for moon in moons)
        if state in seen[axis]:
            axes.remove(axis)
            continue
        seen[axis].add(state)
        for a, b in it.combinations(moons, 2):
            if a.position[axis] > b.position[axis]:
                a.velocity[axis] -= 1
                b.velocity[axis] += 1
            elif a.position[axis] < b.position[axis]:
                a.velocity[axis] += 1
                b.velocity[axis] -= 1
        for moon in moons:
            moon.position[axis] += moon.velocity[axis]

print(reduce(lcm, (len(x) for x in seen)))
