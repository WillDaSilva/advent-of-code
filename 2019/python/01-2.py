with open('01.in') as f:
    data = (int(x) for x in f.readlines())

total = 0
for x in data:
    while (x := x//3 - 2) > 0:
        total += x
print(total)
