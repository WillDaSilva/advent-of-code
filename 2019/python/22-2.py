from functools import partial, reduce

with open('22.in') as f:
    shuffles = (parse_shuffle(x.strip()) for x in f.readlines())

num_cards = 119315717514047
num_repetitions = 101741582076661

def deal_into_new_stack(a, b):
    return (num_cards - a - 1) % num_cards, -b % num_cards

def cut(x, a, b):
    return (a - x) % num_cards, b

def deal_with_increment(x, a, b):
    return (a * x) % num_cards, (b * x) % num_cards

def parse_shuffle(x):
    if x.startswith('cut'):
        return partial(cut, int(x[4:]))
    elif x.startswith('deal with increment'):
        return partial(deal_with_increment, int(x[20:]))
    elif x == 'deal into new stack':
        return deal_into_new_stack

a, b = reduce(lambda x, y: y(*x), shuffles, (0, 1))
x = (pow(1 - b, num_cards - 2, num_cards) * a) % num_cards
y = pow(b, num_repetitions * (num_cards - 2), num_cards)
print((y * (2020 - x) + x) % num_cards)
