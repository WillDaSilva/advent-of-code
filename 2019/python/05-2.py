from collections import deque

with open('05.in') as f:
    data = [int(x) for x in f.readline().split(',')]

def digitize(n):
    while n:
        n, x = divmod(n, 10)
        yield x

valences = {1: 3, 2: 3, 3: 1, 4: 1, 5: 2, 6: 2, 7: 3, 8: 3}

inputs = deque([5])
outputs = []

position = 0
while data[position] != 99:
    digits = list(digitize(data[position]))
    opcode = 10 * (digits[1] if len(digits) > 1 else 0) + digits[0]
    valence = valences[opcode]
    modes = digits[2:] + [0] * (valence - len(digits) + 2)
    parameters = []
    indices = []
    for x in range(1, valence + 1):
        indices.append(None if modes[x-1] == 1 else data[position + x])
        parameters.append(data[position + x] if modes[x-1] == 1 else data[data[position + x]])
    position += valence + 1
    if opcode == 1:
        data[indices[2]] = parameters[0] + parameters[1]
    elif opcode == 2:
        data[indices[2]] = parameters[0] * parameters[1]
    elif opcode == 3:
        data[indices[0]] = inputs.popleft()
    elif opcode == 4:
        outputs.append(parameters[0])
    elif opcode == 5:
        if parameters[0]: position = parameters[1]
    elif opcode == 6:
        if parameters[0] == 0: position = parameters[1]
    elif opcode == 7:
        data[indices[2]] = int(parameters[0] < parameters[1])
    elif opcode == 8:
        data[indices[2]] = int(parameters[0] == parameters[1])
print(outputs)
