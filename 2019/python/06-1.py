from functools import lru_cache

with open('06.in') as f:
    graph = {y:x for x, y in (x.strip().split(')') for x in f.readlines())}

@lru_cache()
def get_depth(node):
    if node in graph:
        return 1 + get_depth(graph[node])
    return 0

print(sum(get_depth(node) for node in graph))
