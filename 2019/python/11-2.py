from collections import defaultdict

with open('11.in') as f:
    program = [int(x) for x in f.readline().split(',')]

def digitize(n):
    while n:
        n, x = divmod(n, 10)
        yield x

def intcode(program):
    program = defaultdict(int, {i:x for i, x in enumerate(program)})
    valences = {1: 3, 2: 3, 3: 1, 4: 1, 5: 2, 6: 2, 7: 3, 8: 3, 9: 1}
    position = 0
    base = 0
    while program[position] != 99:
        digits = list(digitize(program[position]))
        opcode = 10 * (digits[1] if len(digits) > 1 else 0) + digits[0]
        valence = valences[opcode]
        modes = digits[2:] + [0] * (valence - len(digits) + 2)
        parameters = []
        indices = []
        for x in range(1, valence + 1):
            if modes[x-1] == 1:
                indices.append(None)
                parameters.append(program[position + x])
            elif modes[x-1] == 2:
                indices.append(program[position + x] + base)
                parameters.append(program[program[position + x] + base])
            else:
                indices.append(program[position + x])
                parameters.append(program[program[position + x]])
        position += valence + 1
        if opcode == 1:
            program[indices[2]] = parameters[0] + parameters[1]
        elif opcode == 2:
            program[indices[2]] = parameters[0] * parameters[1]
        elif opcode == 3:
            program[indices[0]] = yield
        elif opcode == 4:
            yield parameters[0]
        elif opcode == 5:
            if parameters[0]: position = parameters[1]
        elif opcode == 6:
            if parameters[0] == 0: position = parameters[1]
        elif opcode == 7:
            program[indices[2]] = int(parameters[0] < parameters[1])
        elif opcode == 8:
            program[indices[2]] = int(parameters[0] == parameters[1])
        elif opcode == 9:
            base += parameters[0]
    raise StopIteration()

robot = intcode(program)
hull = defaultdict(int)
x, y = 0, 0
directions = ((0, -1), (1, 0), (0, 1), (-1, 0))
d = 0
hull[(0, 0)] = 1
while True:
    try:
        next(robot)
        hull[(x, y)] = robot.send(hull[(x, y)])
        d = (d + 1 if next(robot) else d - 1) % 4
        x += directions[d][0]
        y += directions[d][1]
    except StopIteration:
        break

min_x = min(x for x, _ in hull)
max_x = max(x for x, _ in hull)
min_y = min(y for _, y in hull)
max_y = max(y for _, y in hull)
for y in range(min_y, max_y + 1):
    print(''.join(' █'[hull[(x, y)]] for x in range(min_x, max_x + 1)))
