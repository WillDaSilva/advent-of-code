with open('08.in') as f:
    data = f.readline().strip()

layers = list(zip(*[iter(data)]*25*6))
pixels = list(zip(*layers))
image = ''.join(next(x for x in p if x != '2') for p in pixels)
for row in zip(*[iter(image)]*25):
    print(''.join(row).translate(str.maketrans({'0': ' ', '1': '█'})))
