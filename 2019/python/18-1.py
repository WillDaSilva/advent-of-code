from collections import deque
from functools import lru_cache

with open('18.in') as f:
    maze = [list(x.strip()) for x in f.readlines()]

class Node:
    def __init__(self, x, y, connected, door = None, key = None):
        self.x = x
        self.y = y
        self.connected = connected
        self.door = door
        self.key = key

def graph_maze(maze):
    graph = {}
    for y in range(len(maze)):
        for x in range(len(maze[0])):
            text = maze[y][x]
            if text == '#':
                continue
            key = door = None
            if text != '.' and text != '@':
                if text.isupper():
                    door = text
                else:
                    key = text
            graph[(x, y)] = Node(x, y, [], door, key)
    for (x, y), node in graph.items():
        for dx, dy in ((0, 1), (0, -1), (1, 0), (-1, 0)):
            if (x + dx, y + dy) in graph.keys():
                node.connected.append(graph[(x + dx, y + dy)])
    return graph

@lru_cache(maxsize=None)
def shortest_path(starting_node, collected_keys=frozenset()):
    keys = {}
    distances = {starting_node: 0}
    q = deque([starting_node])
    while q:
        node = q.popleft()
        for connection in node.connected:
            if connection in distances:
                continue
            distances[connection] = 1 + distances[node]
            if connection.door and connection.door.lower() not in collected_keys:
                continue
            if connection.key and connection.key not in collected_keys:
                keys[connection] = distances[connection]
            q.append(connection)
    if not keys:
        return 0
    print(keys)
    return min(keys[x] + shortest_path(x, collected_keys | {x.key}) for x in keys)

for y in range(len(maze)):
    for x in range(len(maze[0])):
        if maze[y][x] == '@':
            start_x, start_y = x, y

print(shortest_path(graph_maze(maze)[(start_x, start_y)]))
