import itertools as it

with open('24.in') as f:
    grid = [[int(y == '#') for y in x.strip()] for x in f.readlines()]

adjacents = ((0, 1), (0, -1), (1, 0), (-1, 0))
def step(grid):
    new_grid = [row[:] for row in grid]
    for x, y in it.product(range(5), repeat=2):
        n = 0
        for dx, dy in adjacents:
            if y + dy in range(5) and x + dx in range(5):
                n += grid[y + dy][x + dx] 
        if grid[y][x] and n != 1:
            new_grid[y][x] = 0
        elif not grid[y][x] and n in (1, 2):
            new_grid[y][x] = 1
    return new_grid

biodiversity_ratings = list(zip(*[iter([2 ** x for x in range(25)])]*5))
seen = set()
while True:
    b = sum(biodiversity_ratings[y][x] * grid[y][x] for x, y in it.product(range(5), repeat=2))
    if b in seen:
        print(b)
        break
    seen.add(b)
    grid = step(grid)
