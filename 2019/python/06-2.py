with open('06.in') as f:
    graph = {y:x for x, y in (x.strip().split(')') for x in f.readlines())}

def path_to_root(node):
    while node in graph:
        yield (node := graph[node])

you_path = list(path_to_root('YOU'))
san_path = list(path_to_root('SAN'))
first_common_node = next(x for x in you_path if x in set(san_path))
print(you_path.index(first_common_node) + san_path.index(first_common_node))
