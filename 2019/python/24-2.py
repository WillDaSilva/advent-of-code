from collections import defaultdict
from copy import copy
import itertools as it

with open('24.in') as f:
    level_zero = [[int(y == '#') for y in x.strip()] for x in f.readlines()]

grid = defaultdict(int)
for x, y in it.product(range(5), repeat=2):
    if (x, y) != (2, 2):
        grid[x, y, 0] = level_zero[y][x]

def adjacents(x, y, z):
    if y == 0: yield 2 - x, 1, -1
    else:
        if (x, y) == (2, 3):
            yield from ((a - x, 4 - y, 1) for a in range(5))
        else:
            yield 0, -1, 0
    if y == 4: yield 2 - x, -1, -1
    else:
        if (x, y) == (2, 1):
            yield from ((a - x, 0 - y, 1) for a in range(5))
        else:
            yield 0, 1, 0
    if x == 0: yield 1, 2 - y, -1
    else:
        if (x, y) == (3, 2):
            yield from ((4 - x, a - y, 1) for a in range(5))
        else:
            yield -1, 0, 0
    if x == 4: yield -1, 2 - y, -1
    else:
        if (x, y) == (1, 2):
            yield from ((0 - x, a - y, 1) for a in range(5))
        else:
            yield 1, 0, 0
                

def step(grid):
    new_grid = copy(grid)
    levels = set(z for _, _, z in new_grid)
    for x, y in it.product(range(5), repeat=2):
        if (x, y) == (2, 2):
            continue
        for z in levels | {min(levels) - 1, max(levels) + 1}:
            n = sum(grid[x + dx, y + dy, z + dz] for dx, dy, dz in adjacents(x, y, z))
            if grid[x, y, z] and n != 1:
                new_grid[x, y, z] = 0
            elif not grid[x, y, z] and n in (1, 2):
                new_grid[x, y, z] = 1
    return new_grid

for _ in range(200):
    grid = step(grid)
print(sum(grid.values()))