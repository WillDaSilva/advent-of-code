from dataclasses import dataclass
import itertools as it
import re

@dataclass
class Moon:
    position: list
    velocity: list

with open('12.in') as f:
    moons = tuple(Moon([int(y) for y in re.findall(r'-?\d+', x)], [0]*3) for x in f.readlines())

for step in range(1000):
    for a, b in it.combinations(moons, 2):
        for axis in range(3):
            if a.position[axis] > b.position[axis]:
                a.velocity[axis] -= 1
                b.velocity[axis] += 1
            elif a.position[axis] < b.position[axis]:
                a.velocity[axis] += 1
                b.velocity[axis] -= 1
    for moon in moons:
        for axis in range(3):
            moon.position[axis] += moon.velocity[axis]

print(sum(sum(abs(x) for x in moon.position)*sum(abs(x) for x in moon.velocity) for moon in moons))
