from collections import defaultdict
from math import ceil

reactions = {}
with open('14.in') as f:
    def p(x):
        a, b = x.split(' ')
        return (b, int(a))
    for line in f.readlines():
        a, b = line.strip().split(' => ')
        reactions[p(b)[0]] = (p(b)[1], tuple(p(x) for x in a.split(', ')))

def required_ore(fuel):
    required = defaultdict(int, {'FUEL': fuel})
    while True:
        try:
            element, amount_needed = next((x, y) for x, y in required.items() if x != 'ORE' and y > 0)
        except StopIteration:
            break
        output_per_reaction = reactions[element][0]
        num_reactions = ceil(amount_needed / output_per_reaction)
        required[element] -= num_reactions * output_per_reaction
        for reagent in reactions[element][1]:
            required[reagent[0]] += reagent[1] * num_reactions
    return required['ORE']

ore = 1000000000000
a = 1
b = ore
while a < b:
    c = (a + b + 1) // 2
    if required_ore(c) > ore:
        b = c - 1
    else:
        a = c
print(b)