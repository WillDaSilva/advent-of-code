import itertools as it
import numpy as np

with open('16.in') as f:
    signal = np.array([int(x) for x in f.readline().strip()])

pattern = []
for i, x in enumerate(signal, 1):
    pattern.append(list(it.islice(it.cycle([*[0]*i, *[1]*i, *[0]*i, *[-1]*i]), 1, len(signal)+1)))
pattern = np.array(pattern)

for _ in range(100):
    signal = np.abs(pattern.dot(signal)) % 10

signal = ''.join(map(str, signal))
print(signal[:8])
