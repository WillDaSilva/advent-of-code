with open('02.in') as f:
    data = [int(x) for x in f.readline().split(',')]
backup = data[:]

for noun in range(100):
    for verb in range(100):
        data = backup[:]
        data[1] = noun
        data[2] = verb
        position = 0
        while data[position] != 99:
            if data[position] == 1:
                data[data[position + 3]] = data[data[position + 1]] + data[data[position + 2]]
            elif data[position] == 2:
                data[data[position + 3]] = data[data[position + 1]] * data[data[position + 2]]
            position += 4
        if data[0] == 19690720:
            print(100 * noun + verb)
            exit()


