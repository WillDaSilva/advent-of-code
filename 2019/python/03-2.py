from collections import defaultdict

with open('03.in') as f:
    wire_1_instructions = f.readline().strip().split(',')
    wire_2_instructions = f.readline().strip().split(',')

def trace_wire(wire_instructions):
    dirmap = {'R':(1, 0), 'L':(-1, 0), 'U':(0, -1), 'D':(0, 1)}
    x, y = 0, 0
    wire_coords = defaultdict(int)
    wire_length = 0
    for direction, *magnitude in wire_instructions:
        magnitude = int(''.join(magnitude))
        dx, dy = dirmap[direction]
        for _ in range(magnitude):
            x += dx; y += dy
            wire_length += 1
            wire_coords[(x, y)] = wire_length
    return wire_coords

wire_1_map = trace_wire(wire_1_instructions)
wire_2_map = trace_wire(wire_2_instructions)
intersections = set(wire_1_map.keys()) & set(wire_2_map.keys())
print(min(wire_1_map[x] + wire_2_map[x] for x in intersections))
