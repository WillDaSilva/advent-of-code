from functools import partial, reduce

with open('22.in') as f:
    shuffles = (parse_suffle(x.strip()) for x in f.readlines())

cards = list(range(10007))

def cut(x, cards):
    return cards[x:] + cards[:x]

def deal_with_increment(x, cards):
    cards.reverse()
    num_cards = len(cards)
    new_cards = [0]*num_cards
    for i in range(len(cards)):
        new_cards[(i * x) % num_cards] = cards.pop()
    return new_cards

def deal_into_new_stack(cards):
    cards.reverse()
    return cards

def parse_suffle(x):
    if x.startswith('cut'):
        return partial(cut, int(x[4:]))
    elif x.startswith('deal with increment'):
        return partial(deal_with_increment, int(x[20:]))
    elif x == 'deal into new stack':
        return deal_into_new_stack

cards = reduce(lambda x, y: y(x), shuffles, cards)
print(cards.index(2019))
