with open('03.in') as f:
    wire_1_instructions = f.readline().strip().split(',')
    wire_2_instructions = f.readline().strip().split(',')

def trace_wire(wire_instructions):
    dirmap = {'R':(1, 0), 'L':(-1, 0), 'U':(0, -1), 'D':(0, 1)}
    x, y = 0, 0
    wire_coords = set()
    for direction, *magnitude in wire_instructions:
        magnitude = int(''.join(magnitude))
        dx, dy = dirmap[direction]
        for _ in range(magnitude):
            x += dx; y += dy
            wire_coords.add((x, y))
    return wire_coords

intersections = trace_wire(wire_1_instructions) & trace_wire(wire_2_instructions)
print(min(abs(x) + abs(y) for x, y in intersections))
