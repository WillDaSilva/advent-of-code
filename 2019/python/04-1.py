with open('04.in') as f:
    lower, upper = [int(x) for x in f.readline().split('-')]

valid = 0
for x in range(lower, upper + 1):
    s = str(x)
    valid += len(set(s))<len(s) and sorted(s) == list(s)
print(valid)