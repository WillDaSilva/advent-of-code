import itertools as it
from collections import deque

with open('07.in') as f:
    program = [int(x) for x in f.readline().split(',')]

def digitize(n):
    while n:
        n, x = divmod(n, 10)
        yield x

def intcode(program, inputs):
    valences = {1: 3, 2: 3, 3: 1, 4: 1, 5: 2, 6: 2, 7: 3, 8: 3}
    inputs = deque(inputs)
    outputs = []
    position = 0
    while program[position] != 99:
        digits = list(digitize(program[position]))
        opcode = 10 * (digits[1] if len(digits) > 1 else 0) + digits[0]
        valence = valences[opcode]
        modes = digits[2:] + [0] * (valence - len(digits) + 2)
        parameters = []
        indices = []
        for x in range(1, valence + 1):
            indices.append(None if modes[x-1] == 1 else program[position + x])
            parameters.append(program[position + x] if modes[x-1] == 1 else program[program[position + x]])
        position += valence + 1
        if opcode == 1:
            program[indices[2]] = parameters[0] + parameters[1]
        elif opcode == 2:
            program[indices[2]] = parameters[0] * parameters[1]
        elif opcode == 3:
            program[indices[0]] = inputs.popleft()
        elif opcode == 4:
            outputs.append(parameters[0])
        elif opcode == 5:
            if parameters[0]: position = parameters[1]
        elif opcode == 6:
            if parameters[0] == 0: position = parameters[1]
        elif opcode == 7:
            program[indices[2]] = int(parameters[0] < parameters[1])
        elif opcode == 8:
            program[indices[2]] = int(parameters[0] == parameters[1])
    return outputs[-1]

max_e = 0
for a, b, c, d, e in it.permutations(range(5), 5):
        e_value = intcode(program[:], [e,
            intcode(program[:], [d,
                intcode(program[:], [c,
                    intcode(program[:], [b,
                        intcode(program[:], [a, 0])])])])])
        if e_value > max_e:
            max_e = e_value
print(max_e)