import itertools as it
from collections import defaultdict

with open('13.in') as f:
    program = [int(x) for x in f.readline().split(',')]

def digitize(n):
    while n:
        n, x = divmod(n, 10)
        yield x

def intcode(program):
    program = defaultdict(int, {i:x for i, x in enumerate(program)})
    valences = {1: 3, 2: 3, 3: 1, 4: 1, 5: 2, 6: 2, 7: 3, 8: 3, 9: 1}
    position = 0
    base = 0
    while program[position] != 99:
        digits = list(digitize(program[position]))
        opcode = 10 * (digits[1] if len(digits) > 1 else 0) + digits[0]
        valence = valences[opcode]
        modes = digits[2:] + [0] * (valence - len(digits) + 2)
        parameters = []
        indices = []
        for x in range(1, valence + 1):
            if modes[x-1] == 1:
                indices.append(None)
                parameters.append(program[position + x])
            elif modes[x-1] == 2:
                indices.append(program[position + x] + base)
                parameters.append(program[program[position + x] + base])
            else:
                indices.append(program[position + x])
                parameters.append(program[program[position + x]])
        position += valence + 1
        if opcode == 1:
            program[indices[2]] = parameters[0] + parameters[1]
        elif opcode == 2:
            program[indices[2]] = parameters[0] * parameters[1]
        elif opcode == 3:
            program[indices[0]] = yield
        elif opcode == 4:
            yield parameters[0]
        elif opcode == 5:
            if parameters[0]: position = parameters[1]
        elif opcode == 6:
            if parameters[0] == 0: position = parameters[1]
        elif opcode == 7:
            program[indices[2]] = int(parameters[0] < parameters[1])
        elif opcode == 8:
            program[indices[2]] = int(parameters[0] == parameters[1])
        elif opcode == 9:
            base += parameters[0]
    raise StopIteration()

c = intcode(program)
a = 0
try:
    while True:
        x = next(c)
        y = next(c)
        tid = next(c)
        a += tid == 2
except StopIteration:
    pass
print(a)