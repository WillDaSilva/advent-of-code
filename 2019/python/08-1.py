with open('08.in') as f:
    data = f.readline().strip()

layers = list(zip(*[iter(data)]*25*6))
index = min(((sum(y == '0' for y in x), i) for i, x in enumerate(layers)))[1]
print(sum(x == '1' for x in layers[index]) * sum(x == '2' for x in layers[index]))
