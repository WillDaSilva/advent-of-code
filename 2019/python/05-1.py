from collections import deque

with open('05.in') as f:
    data = [int(x) for x in f.readline().split(',')]

def digitize(n):
    while n:
        n, x = divmod(n, 10)
        yield x

valences = {1: 3, 2: 3, 3: 1, 4: 1}

inputs = deque([1])
outputs = []

position = 0
while data[position] != 99:
    digits = list(digitize(data[position]))
    opcode = 10 * (digits[1] if len(digits) > 1 else 0) + digits[0]
    valence = valences[opcode]
    modes = digits[2:] + [0] * (valence - len(digits) + 2)
    parameters = []
    indices = []
    for x in range(1, valence + 1):
        indices.append(None if modes[x-1] == 1 else data[position + x])
        parameters.append(data[position + x] if modes[x-1] == 1 else data[data[position + x]])
    if opcode == 1:
        data[indices[2]] = parameters[0] + parameters[1]
    elif opcode == 2:
        data[indices[2]] = parameters[0] * parameters[1]
    elif opcode == 3:
        data[indices[0]] = inputs.popleft()
    elif opcode == 4:
        outputs.append(parameters[0])
    position += valence + 1
print(outputs)
