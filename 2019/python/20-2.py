from collections import deque, defaultdict
from string import ascii_uppercase

with open('20.in') as f:
    maze = [list(x.strip('\n')) for x in f.readlines()]

class Node:
    def __init__(self, x, y, connected, portal, portal_destination):
        self.x = x
        self.y = y
        self.connected = connected
        self.portal = portal
        self.portal_destination = portal_destination

def build_portal(maze, x, y):
    if maze[y + 1][x] in ascii_uppercase: # below
        return maze[y][x] + maze[y + 1][x]
    elif maze[y - 1][x] in ascii_uppercase: # above
        return maze[y - 1][x] + maze[y][x]
    elif maze[y][x + 1] in ascii_uppercase: # right
        return maze[y][x] + maze[y][x + 1]
    elif maze[y][x - 1] in ascii_uppercase: # left
        return maze[y][x - 1] + maze[y][x]

start_xy = None
def graph_maze(maze):
    adjacents = ((0, 1), (0, -1), (1, 0), (-1, 0))
    portals = defaultdict(set)
    graph = {}
    for y in range(len(maze)):
        for x in range(len(maze[0])):
            text = maze[y][x]
            if text != '.':
                continue
            portal = None
            for dx, dy in adjacents:
                if maze[y + dy][x + dx] in ascii_uppercase:
                    portal = build_portal(maze, x + dx, y + dy)
                    if portal == 'AA':
                        global start_xy
                        start_xy = (x, y)
            graph[(x, y)] = Node(x, y, set(), portal, None)
            if portal and portal != 'AA' and portal != 'ZZ':
                portals[portal].add(graph[(x, y)])
    for portal, (a, b) in portals.items():
        a.portal_destination = b
        b.portal_destination = a
    for (x, y), node in graph.items():
        for dx, dy in adjacents:
            if (x + dx, y + dy) in graph.keys():
                node.connected.add(graph[(x + dx, y + dy)])
    return graph

q = deque([(graph_maze(maze)[start_xy], 0, 0)])
seen = {}
while q:
    node, steps, depth = q.popleft()
    if (node, depth) in seen:
        continue
    if node.portal == 'ZZ' and depth == 0:
        print(steps)
        break
    seen[node, depth] = steps
    q.extend((x, steps + 1, depth) for x in node.connected)
    if node.portal and node.portal != 'AA' and node.portal != 'ZZ':
        if 34 <= node.x <= 92 and 34 <= node.y <= 86:
            q.append((node.portal_destination, steps + 1, depth + 1))
        elif depth >= 1:
            q.append((node.portal_destination, steps + 1, depth - 1))
