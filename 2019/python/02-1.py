with open('02.in') as f:
    data = [int(x) for x in f.readline().split(',')]

data[1] = 12
data[2] = 2

position = 0
while data[position] != 99:
    if data[position] == 1:
        data[data[position + 3]] = data[data[position + 1]] + data[data[position + 2]]
    elif data[position] == 2:
        data[data[position + 3]] = data[data[position + 1]] * data[data[position + 2]]
    position += 4
print(data[0])


