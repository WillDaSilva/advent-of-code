from collections import deque
from functools import lru_cache
import itertools as it

with open('18.in') as f:
    maze = [list(x.strip()) for x in f.readlines()]

class Node:
    def __init__(self, x, y, connected, door = None, key = None):
        self.x = x
        self.y = y
        self.connected = connected
        self.door = door
        self.key = key

def graph_maze(maze):
    graph = {}
    for y in range(len(maze)):
        for x in range(len(maze[0])):
            text = maze[y][x]
            if text == '#':
                continue
            key = door = None
            if text != '.' and text != '@':
                if text.isupper():
                    door = text
                else:
                    key = text
            graph[(x, y)] = Node(x, y, [], door, key)
    for (x, y), node in graph.items():
        for dx, dy in ((0, 1), (0, -1), (1, 0), (-1, 0)):
            if (x + dx, y + dy) in graph.keys():
                node.connected.append(graph[(x + dx, y + dy)])
    return graph

@lru_cache(maxsize=None)
def shortest_path(starting_nodes, collected_keys=frozenset()):
    keys = {}
    for i, starting_node in enumerate(starting_nodes):
        q = deque([starting_node])
        distances = {starting_node: 0}
        while q:
            node = q.popleft()
            for connection in node.connected:
                if connection in distances:
                    continue
                distances[connection] = 1 + distances[node]
                if connection.door and connection.door.lower() not in collected_keys:
                    continue
                if connection.key and connection.key not in collected_keys:
                    keys[connection] = (distances[connection], i)
                q.append(connection)
    if not keys:
        return 0
    num_steps = []
    for node, (distance, i) in keys.items():
        sp = shortest_path(tuple((x, node)[i == j] for j, x in enumerate(starting_nodes)), collected_keys | {node.key})
        num_steps.append(distance + sp)
    return min(num_steps)

for y in range(len(maze)):
    for x in range(len(maze[0])):
        if maze[y][x] == '@':
            start_x, start_y = x, y
for dy, dx in it.product(range(-1, 2), repeat=2):
    maze[start_y + dy][start_x + dx] = '@#@###@#@'[(dy + 1) * 3 + dx + 1]
maze_graph = graph_maze(maze)
new_starts = tuple(maze_graph[(start_x + dx, start_y + dy)] for dx, dy in ((-1, -1), (-1, 1), (1, -1), (1, 1)))
print(shortest_path(new_starts))
