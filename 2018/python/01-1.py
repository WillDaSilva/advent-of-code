with open('01.in') as puzzleInputFile:
    puzzleInput = puzzleInputFile.readlines()

print(sum(int(x) for x in puzzleInput))
