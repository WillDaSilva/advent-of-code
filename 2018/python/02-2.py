from operator import eq

with open('02.in') as puzzleInputFile:
    puzzleInput = puzzleInputFile.readlines()

for i, x in enumerate(puzzleInput):
    for y in puzzleInput[i:]:
        matches = [eq(*z) for z in zip(x, y)]
        if len(x) - 1 == sum(matches):
            differenceIdx = [j for j, a in enumerate(matches) if not a][0]
            print(''.join((x[:differenceIdx], x[differenceIdx+1:])))
