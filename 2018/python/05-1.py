with open('05.in') as puzzleInputFile:
    polymer = puzzleInputFile.readline().strip()

reduced = []
for x in polymer:
    if reduced and x == reduced[-1].swapcase():
        reduced.pop()
    else:
        reduced.append(x)
print(len(reduced))

