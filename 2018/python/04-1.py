from collections import Counter, defaultdict
import itertools as it
import re

with open('04.in') as puzzleInputFile:
    puzzleInput = sorted(puzzleInputFile.readlines())

def parseRecord(x):
    return [
        int(x[2] if not int(x[1]) else 0),  # minute
        bool(x[3][0] == 'f'),               # action
        int(x[4] if x[4] else 0),           # guard ID
        int(x[0]) + bool(int(x[1]))         # day
        ]

pattern = r'\[.*?(\d+) (\d+):(\d+)\] (\w+ [\w#]?(\d+)?)'
records = [parseRecord(re.findall(pattern, x)[0]) for x in puzzleInput]
for record in records: guardID = record[2] = record[2] or guardID
grouped = it.groupby(records, lambda x: x[2:])
records = ((x[0], sorted(y[0] for y in x[1])) for x in grouped)

sleepTracker = defaultdict(Counter)
for shift, times in records:
    times.append(60)
    sleepRanges = (range(*x) for x in zip(times[1::2], times[2::2]))
    sleepTracker[shift[0]] += Counter(it.chain.from_iterable(sleepRanges))

guardID = max((sum(c.values()), guardID) for guardID, c in sleepTracker.items())[1]
print(guardID * sleepTracker[guardID].most_common()[0][0])
