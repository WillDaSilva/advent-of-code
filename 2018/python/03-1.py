from collections import Counter
import re

with open('03.in') as puzzleInputFile:
    puzzleInput = puzzleInputFile.readlines()

claims = (re.findall(r'#\d+ @ (\d+),(\d+): (\d+)x(\d+)', x)[0] for x in puzzleInput)
claims = (tuple(int(y) for y in x) for x in claims)

fabric = Counter()
for claim in claims:
    x, y, w, h = claim
    fabric.update({(x+a, y+b) for a in range(w) for b in range(h)})

print(sum(x >= 2 for x in fabric.values()))
