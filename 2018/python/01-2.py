import itertools as it

with open('01.in') as puzzleInputFile:
    puzzleInput = puzzleInputFile.readlines()

seen = set()
frequency = 0
for x in it.cycle(int(x) for x in puzzleInput):
    frequency += x
    if frequency in seen: break
    seen.add(frequency)
print(frequency)
