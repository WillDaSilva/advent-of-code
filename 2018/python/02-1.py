from collections import Counter
from operator import mul

with open('02.in') as puzzleInputFile:
    puzzleInput = puzzleInputFile.readlines()

tallies = (Counter(x).values() for x in puzzleInput)
print(mul(*(sum(y) for y in zip(*((2 in x, 3 in x) for x in tallies)))))
