from multiprocessing import Pool
from string import ascii_lowercase as agents

with open('05.in') as puzzleInputFile:
    polymer = puzzleInputFile.readline().strip()

def react(polymer):
    reduced = []
    for x in polymer:
        if reduced and x == reduced[-1].swapcase():
            reduced.pop()
        else:
            reduced.append(x)
    return len(reduced)

trials = (polymer.replace(x, '').replace(x.upper(), '') for x in agents)
with Pool() as pool:
    print(min(pool.map_async(react, trials).get()))

