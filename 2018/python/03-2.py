from collections import defaultdict
import re

with open('03.in') as puzzleInputFile:
    puzzleInput = puzzleInputFile.readlines()

claims = (re.findall(r'#(\d+) @ (\d+),(\d+): (\d+)x(\d+)', x)[0] for x in puzzleInput)
claims = [tuple(int(y) for y in x) for x in claims]

isContested = defaultdict(bool)
fabric = dict()
for claim in claims:
    i, x, y, w, h = claim
    tiles = {(x+a, y+b) for a in range(w) for b in range(h)}
    for tile in tiles:
        if tile in fabric:
            isContested[i] = isContested[fabric[tile]] = True
        fabric[tile] = i

print((set(range(1, len(claims) + 1)) - isContested.keys()).pop())
